import pandas as pd

# area_map.csv 파일 읽기
area_map_df = pd.read_csv('area_map.csv')

# area_struct.csv 파일 읽기
area_struct_df = pd.read_csv('area_struct.csv')

# struct_category.csv 파일 읽기
struct_category_df = pd.read_csv('area_category.csv')

# struct_category를 dictionary로 변환
struct_category_dict = dict(zip(struct_category_df['category'], struct_category_df['struct']))

# area_struct_df에 category를 이름으로 변환하여 추가
area_struct_df['category_name'] = area_struct_df['category'].map(struct_category_dict)

# area 1에 해당하는 데이터 필터링
area_1_data = area_struct_df[area_struct_df['area'] == 1]

# 결과 출력
print("Area 1에 설치된 시설:")
print(area_1_data)
